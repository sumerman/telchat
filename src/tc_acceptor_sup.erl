%%%-------------------------------------------------------------------
%%% @author Meleshkin Valery
%%% @copyright 2013 T-Platforms
%%%-------------------------------------------------------------------

-module(tc_acceptor_sup).

-behaviour(supervisor).

%% API
-export([start_link/4]).

%% Supervisor callbacks
-export([init/1]).

-include("telchat_defs.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

-spec start_link(non_neg_integer(), inet:port_number(),
                 [?TRANSPORT:listen_option()], 
                 {module(), atom(), [term()]}) -> {ok, pid()}.
start_link(NbAcceptors, Port, TransOpts, Callback) ->
  supervisor:start_link({local, ?MODULE}, 
                        ?MODULE, [NbAcceptors, Port, TransOpts, Callback]).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([NbAcceptors, Port, TransOpts, Callback]) ->
  {ok, LSocket} = ?TRANSPORT:listen(Port, [{active, false} | TransOpts]),
  {ok, {_, Port}} = inet:sockname(LSocket),
  error_logger:info_msg("Listening on ~p", [Port]),
  ChSpec = [
      {{acceptor_proc, N}, 
       {tc_acceptor, start_link, [LSocket, Callback]},
       permanent, brutal_kill, worker, []}
      || N <- lists:seq(1, NbAcceptors)],
  {ok, {{one_for_one, 10, 10}, ChSpec}}.
