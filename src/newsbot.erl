%%%-------------------------------------------------------------------
%%% @author Meleshkin Valery
%%% @copyright 2013 T-Platforms
%%%-------------------------------------------------------------------

-module(newsbot).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
%-compile(export_all).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/2]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, 
         handle_info/2, terminate/2, code_change/3]).

-record(state, {sock}).
-define(NAME, "News").
-define(FEED, "http://news.yandex.ru/index.rss").
-define(FEED_LEN, 5).
-include_lib("xmerl/include/xmerl.hrl").

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link(Host, Port) ->
  inets:start(),
  gen_server:start_link({local, ?SERVER}, ?MODULE, [Host, Port], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([Host, Port]) ->
  self() ! {connect, Host, Port},
  {ok, #state{}}.

handle_call(_Request, _From, State) ->
  {noreply, ok, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info({tcp, Sock, Data}, State) ->
  case re:run(Data, "(@" ++ ?NAME ++ ")\s*(latest)?", [{capture, all_but_first}]) of
    {match, [_Name, _Cmd]} ->
      case catch get_news() of
        [_|_] = News ->
          gen_tcp:send(Sock, "Here you are:\n"),
          gen_tcp:send(Sock, News);
        _Else -> 
          gen_tcp:send(Sock, "Unable to fetch :(\n")
      end;
    {match, [_Name]} ->
      gen_tcp:send(Sock, "WAT?\n");
    nomatch ->
      ok
  end,
  {noreply, State};
handle_info({tcp_closed, _Sock}, State) ->
  {stop, normal, State};
handle_info({connect, Host, Port}, State) ->
  {ok, Sock} = gen_tcp:connect(Host, Port, 
                               [{active, true}, 
                                {mode, binary}, 
                                {packet, line}]),
  ok = gen_tcp:send(Sock, [?NAME, "\r\n"]),
  {noreply, State#state{sock=Sock}}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

get_news() -> 
  case httpc:request(?FEED) of
    {ok,{{_Version,Code,_Res},
         _Headers,Body}} when Code >= 200, Code < 300 ->
      {Doc, _Rest} = xmerl_scan:string(Body),
      Titles = lists:map(fun text_val/1,
                         xmerl_xpath:string("/rss/channel/item/title/text()", Doc)),
      Times = lists:map(fun text_val/1,
                        xmerl_xpath:string("/rss/channel/item/pubDateUT/text()", Doc)),
      RSS = lists:sort(fun({_,T1}, {_,T2}) -> 
              list_to_integer(T1) =< list_to_integer(T2)
          end, lists:zip(Titles, Times)),
      [unicode:characters_to_binary([V, $\n]) ||
        {V, _T} <- lists:sublist(lists:reverse(RSS), ?FEED_LEN)];
    {error, _Reson} -> 
      []
  end.

text_val(#xmlText{value=V}) -> V.

