%%%-------------------------------------------------------------------
%%% @author Meleshkin Valery
%%% @copyright 2013 T-Platforms
%%%-------------------------------------------------------------------

-module(tc_acceptor).

-export([start_link/2]).

-export([loop/2]).

-include("telchat_defs.hrl").

-spec start_link(inet:socket(), {module(), atom(), [term()]}) -> no_return().
start_link(ListenSock, CallbackMFA) ->
  {ok, proc_lib:spawn_link(?MODULE, loop, [ListenSock, CallbackMFA])}.

-spec loop(inet:socket(), {module(), atom(), [term()]}) -> no_return().
loop(ListenSock, {M, F, A} = CallbackMFA) ->
  case ?TRANSPORT:accept(ListenSock, infinity) of
    {ok, ConnSocket} ->
      {ok, Pid} = apply(M, F, A),
      ?TRANSPORT:controlling_process(ConnSocket, Pid),
      Pid ! {connection, ConnSocket};
    {error, emfile} ->
      % wait for fd
      receive after 100 -> ok end;
    % crash upon ListenSock got closed.
    {error, Reason} when Reason =/= closed ->
      ok
  end,
  ?MODULE:loop(ListenSock, CallbackMFA).
