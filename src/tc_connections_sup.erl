%%%-------------------------------------------------------------------
%%% @author Meleshkin Valery
%%% @copyright 2013 T-Platforms
%%%-------------------------------------------------------------------

-module(tc_connections_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start_connection/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILDW(I, Opts), 
        {I, {I, start_link, Opts}, 
         temporary, brutal_kill, worker, [I]}).


%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_connection() ->
  supervisor:start_child(?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  {ok, { {simple_one_for_one, 5, 10}, 
        [?CHILDW(tc_connection, [])]} }.

