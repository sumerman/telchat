
-module(telchat_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Args), 
        {I, {I, start_link, Args}, 
         permanent, infinity, supervisor, [I]}).

-include("telchat_defs.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  {ok, Port} = application:get_env(port),
  {ok, { {one_for_one, 5, 10}, [
        ?CHILD(tc_connections_sup, []),
        ?CHILD(tc_acceptor_sup, 
               [?ACCEPTORS_COUNT, Port, [], 
                {tc_connections_sup, start_connection, []}])
        ]} }.

