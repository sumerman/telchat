%%%-------------------------------------------------------------------
%%% @author Meleshkin Valery
%%% @copyright 2013 T-Platforms
%%%-------------------------------------------------------------------

-module(tc_connection).
-behaviour(gen_fsm).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

%% ------------------------------------------------------------------
%% gen_fsm Function Exports
%% ------------------------------------------------------------------

-export([init/1, anonymous/2,
         handle_event/3,handle_sync_event/4, 
         handle_info/3, terminate/3, code_change/4]).

-include("telchat_defs.hrl").
-define(ROOM, {?MODULE, room}).
-define(USER(Name), {?MODULE, Name}).
-define(USERNAME_RE, "([^\s\r\n]+)").
-define(MENTION, $@).
-define(COMMAND, $!).
-record(state, {sock, username}).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
  gen_fsm:start_link(?MODULE, [], []).

send_msg(Target, Message) when is_list(Message) ->
  send_msg(Target, iolist_to_binary(Message));
send_msg(Pid, Message) when is_pid(Pid) ->
  gen_fsm:send_all_state_event(Pid, {message, Message});
send_msg(?ROOM, Message) ->
  [send_msg(Pid, Message) || Pid <- pg2:get_members(?ROOM)];
send_msg(?USER(User) = RegName, Message) ->
  case global:whereis_name(RegName) of
    undefined ->
      send_msg(self(), ["There is no user with name: ", User]);
    Pid when is_pid(Pid) ->
      send_msg(Pid, Message)
  end.

%% ------------------------------------------------------------------
%% gen_fsm Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
  pg2:create(?ROOM),
  {ok, no_connection, #state{}}.

anonymous(timeout, State) ->
  send_msg(self(), "Enter your username pls:"),
  {next_state, anonymous, State}.

handle_event({message, Msg}, StateName, State) ->
  ok = ?TRANSPORT:send(State#state.sock, append_nl(Msg)),
  {next_state, StateName, State}.

handle_sync_event(_Event, _From, StateName, State) ->
  {reply, ok, StateName, State}.

handle_info({tcp_closed, _S}, online, State) ->
  {stop, normal, State};
handle_info({tcp, _S, Data}, online, State) ->
  ok = inet:setopts(State#state.sock, [{active, once}]),
  case Data of
    <<?MENTION, _Rest/binary>> -> 
      {ok, User, _Message} = explode_mention(Data),
      send_msg(?USER(User), prepend_sender_name(State, Data)),
      send_msg(self(), prepend_sender_name(State, Data));
    <<?COMMAND, Command/binary>> -> 
      handle_chat_command(State, Command);
    Message ->
      send_msg(?ROOM, prepend_sender_name(State, Message))
  end,
  {next_state, online, State};

handle_info({tcp, _S, Data}, anonymous, #state{sock=Sock} = State) ->
  ok = inet:setopts(Sock, [{active, once}]),
  {ok, Username} = ensure_username(Data), %TODO
  case global:register_name(?USER(Username), self()) of
    yes ->
      ok = pg2:join(?ROOM, self()),
      send_msg(?ROOM, ["User '", Username, "' has joined the room"]),
      {next_state, online, State#state{username=Username}};
    no -> 
      send_msg(self(),
               ["Unfortunately name '", Username, "' is already in use."]),
      {next_state, anonymous, State, 0}
  end;
handle_info({connection, Sock}, no_connection, State) ->
  ok = inet:setopts(Sock, [{active, once}, 
                           {mode, binary}, 
                           {nodelay, true}, 
                           {packet, line}, 
                           {packet_size, ?PACKSIZE}]),
  {next_state, anonymous, State#state{sock=Sock}, 0}.

terminate(_Reason, _StateName, _State) ->
  ok.

code_change(_OldVsn, StateName, State, _Extra) ->
  {ok, StateName, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

prepend_sender_name(State, Message) ->
  [State#state.username, <<":> ">>, Message].

ensure_username(Data) ->
  case re:run(Data, ?USERNAME_RE, [{capture, all_but_first, binary}]) of
    {match, [Username]} -> 
      {ok, Username};
    nomatch -> error
  end.

explode_mention(Data) ->
  case re:run(Data, [?MENTION | ?USERNAME_RE] ++ "(.*)", [{capture, all_but_first, binary}]) of
    {match, [Username, Message]} -> {ok, Username, Message};
    nomatch -> error
  end.

append_nl(Data) when is_binary(Data) ->
  Sz = (byte_size(Data) - 1),
  case Data of
    <<_:Sz/binary, $\n>> -> Data;
    _Else -> [Data, $\n]
  end;
append_nl(Data) ->
  append_nl(iolist_to_binary(Data)).

handle_chat_command(State, <<"q", _Rest/binary>>) ->
  handle_chat_command(State, <<"exit">>);
handle_chat_command(State, <<"exit", _Rest/binary>>) ->
  send_msg(?ROOM, prepend_sender_name(State, "leaving...")),
  exit(normal);
handle_chat_command(_State, <<"l", _Rest/binary>>) ->
  NamesList = [[User,$\n] || ?USER(User) <- global:registered_names()],
  send_msg(self(), NamesList);
handle_chat_command(_State, Unkonwn) ->
  send_msg(self(), ["Unknown command: ", Unkonwn]).

