-module(telchat_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
  application:start(telchat).

start(_StartType, _StartArgs) ->
  telchat_sup:start_link().

stop(_State) ->
  ok.
