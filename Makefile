REBAR=`which rebar || echo ./rebar`
.PHONY: all compile clean eunit 
DIRS=src 

all: compile

compile: 
	$(REBAR) compile

clean:
	$(REBAR) clean skip_deps=true
	-rm riak.plt
	-rm log/*

eunit:
	$(REBAR) skip_deps=true eunit

tags:
	erl -s tags subdir "./" -s init stop -noshell

dialyzer: compile
	dialyzer --plts ~/.dialyzer_plt --fullpath ebin -Wunmatched_returns -Werror_handling -Wrace_conditions

